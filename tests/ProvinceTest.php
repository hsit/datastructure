<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use HSIT\DataStructure\Province;

class ProvinceTest extends TestCase{
	public static function dataProvider(): array{
		return [
			['RM', 'Roma'],
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewProvince(string $code, string $name): void{
		$obj = new Province($code, $name);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\HSIT\\DataStructure\\Province", $obj);

	}
}



?>

