<?php
declare(strict_types =1 );
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use HSIT\DataStructure\Istat;

class IstatTest extends TestCase{
	public static function dataProvider(): array{
		return [
			[12342],
		];
	}

	/**
	 * @dataProvider dataProvider
	 */
	public function testNewIstat(int $code): void{
		$obj = new Istat($code);
		$this->assertIsObject($obj);
		$this->assertInstanceOf("\\HSIT\\DataStructure\\Istat", $obj);

	}
}



?>

