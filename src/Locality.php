<?php
namespace HSIT\DataStructure;

use Fdsn\DataStructure\LatLon;

use HSIT\DataStructure\Istat;

/**
 * Data structure to handle Locality
 *
 * @param string $name		Name of location
 * @param Istat $istat		Istat obj for location info
 * @param LatLon $point|null	LatLon of epicenter		
 * @param Province $province|null 	Province obj
 *
 * @return true, if every check is passed, false otherwise
 */

class Locality {
	private string $name;
	private Istat $istat;
	private LatLon $point;
	private Province $province;

	function __construct(string $name, Istat $istat = null, LatLon $point = null, Province $province = null) {

		if( empty($name) )
			throw new \InvalidArgumentException("Location name invalid");

		$this->name = $name;
		$this->istat = $istat;
		$this->point = $point;
		$this->province = $province;
	}

	function __destruct(){ }

	function __toString(){ 
		return sprintf("%s, istat %s, coords: %s, province: %s", 
			$this->name, 
			$this->istat->code(),
			$this->point,
			$this->province->code()); 
	}

	public function name():string { return $this->name; }
	public function point():LatLon { return $this->point; }
	public function istat():Istat { return $this->istat; }
	public function province():Province { return $this->province; }
} 




?>
