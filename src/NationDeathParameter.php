<?php
namespace HSIT\DataStructure;

/**
 * Data structure to handle NationDeathParameter info 
 *
 * @param float $beta
 * @param float $teta
 * @param float $zeta
 *
 * @return true, if every check is passed, false otherwise
 */

class NationDeathParameter {
	private float $beta;
	private float $teta;
	private float $zeta;

	function __construct(float $beta,
				float $teta,
				float $zeta){
		$this->beta = $beta;
		$this->teta = $teta;
		$this->zeta = $zeta;
	}

	function __destruct(){ }

	function __toString(){ 
		return sprintf("beta: %.2f, teta: %.2f, zeta: %.2f", 
			$this->beta, 
			$this->teta, 
			$this->zeta); 
	}

	public function beta():float { return $this->beta; }
	public function teta():float { return $this->teta; }
	public function zeta():float { return $this->zeta; }
} 




?>
