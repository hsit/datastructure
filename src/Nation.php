<?php
namespace HSIT\DataStructure;

use HSIT\DataStructure\NationEconomicParameter;
use HSIT\DataStructure\NationDeathParameter;
use HSIT\DataStructure\Pil;

/**
 * Data structure to handle Nation info 
 *
 * @param string $code	Nation code (2 letters)
 * @param string $name 	Nation name
 * @param NationEconomicParameter|null $economicParams
 * @param NationDeathParameter|null $deathParams
 * @param Pil $personalPil
 *
 * @return true, if every check is passed, false otherwise
 */

class Nation {
	private string $code;
	private string $name;
	private string $HTMLOption;
	private NationEconomicParameter $economicParams;
	private NationDeathParameter $deathParams;
	private Pil $personalPil;

	function __construct(string $code, 
				string $name,
				string $HTMLOption,
				NationEconomicParameter $economicParams,
				NationDeathParameter $deathParams,
				Pil $personalPil) {

		if( empty($code) )
			throw new \InvalidArgumentException("Nation code invalid");

		if( empty($name) )
			throw new \InvalidArgumentException("Nation name invalid");

		if( empty($HTMLOption) )
			throw new \InvalidArgumentException("Nation HTMLOption invalid");

		if (! $this->isValidCode($code)) 
			throw new \InvalidArgumentException("Code invalid");

		if (! $this->isValidName($name)) 
			throw new \InvalidArgumentException("Name invalid");

		if (! $this->isValidName($HTMLOption)) 
			throw new \InvalidArgumentException("HTMLOption invalid");

		$this->code = $code;
		$this->name = $name;
		$this->HTMLOption = $HTMLOption;
		$this->economicParams = $economicParams;
		$this->deathParams = $deathParams;
		$this->personalPil = $personalPil;
	}

	function __destruct(){ }

	function __toString(){ 
		return sprintf("code %s, name %s", 
			$this->code, $this->name); 
	}

	private function isValidCode($code):bool { return preg_match('/^[A-Z]{2}$/', $code); }
	private function isValidName($name):bool { return preg_match('/^\p{L}+$/ui', $name); } // https://stackoverflow.com/questions/31391516/preg-match-words-with-accents

	public function code():string { return $this->code; }
	public function name():string { return $this->name; }
	public function HTMLOption():string { return $this->HTMLOption; }
	public function economicParameters():NationEconomicParameter { return $this->economicParams; }
	public function deathParameters():NationDeathParameter{ return $this->deathParams; }
	public function personalPIL():Pil{ return $this->personalPil; }
} 




?>
