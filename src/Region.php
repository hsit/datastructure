<?php
namespace HSIT\DataStructure;

/**
 * Data structure to handle Region info 
 *
 * @param string $name 	Region name
 *
 * @return true, if every check is passed, false otherwise
 */

class Region {
	private string $name;

	function __construct(string $name) {

		if( empty($name) )
			throw new \InvalidArgumentException("Region name invalid");

		if (! $this->isValidName($name)) 
			throw new \InvalidArgumentException("Name invalid");

		$this->name = $name;
	}

	function __destruct(){ }

	function __toString(){ return sprintf("%s", $this->name); }

	private function isValidName($name):bool { return true; } // https://stackoverflow.com/questions/31391516/preg-match-words-with-accents

	public function name():string { return $this->name; }
} 




?>
