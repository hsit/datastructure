<?php
namespace HSIT\DataStructure;

/**
 * Data structure to handle NationEconomicParameter info 
 *
 * @param float $alfa
 * @param float $beta
 * @param float $teta
 * @param float $zeta
 *
 * @return true, if every check is passed, false otherwise
 */

class NationEconomicParameter {
	private float $alfa;
	private float $beta;
	private float $teta;
	private float $zeta;

	function __construct(float $alfa,
				float $beta,
				float $teta,
				float $zeta){
		$this->alfa = $alfa;
		$this->beta = $beta;
		$this->teta = $teta;
		$this->zeta = $zeta;
	}

	function __destruct(){ }

	function __toString(){ 
		return sprintf("alfa: %.2f, beta: %.2f, teta: %.2f, zeta: %.2f", 
			$this->alfa, 
			$this->beta, 
			$this->teta, 
			$this->zeta); 
	}

	public function alfa():float { return $this->alfa; }
	public function beta():float { return $this->beta; }
	public function teta():float { return $this->teta; }
	public function zeta():float { return $this->zeta; }
} 




?>
