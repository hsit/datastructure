<?php
namespace HSIT\DataStructure;

/**
 * Data structure to handle Province info 
 *
 * @param string $code	Province code (2 letters)
 * @param string $name 	Province name
 *
 * @return true, if every check is passed, false otherwise
 */

class Province {
	private string $code;
	private string $name;

	function __construct(string $code, string $name) {

		if( empty($code) )
			throw new \InvalidArgumentException("Province code invalid");

		if( empty($name) )
			throw new \InvalidArgumentException("Province name invalid");

		if (! $this->isValidCode($code)) 
			throw new \InvalidArgumentException("Code invalid");

		if (! $this->isValidName($name)) 
			throw new \InvalidArgumentException("Name invalid");

		$this->code = $code;
		$this->name = $name;
	}

	function __destruct(){ }

	function __toString(){ 
		return sprintf("code %s, name %s", 
			$this->code, $this->name); 
	}

	private function isValidCode($code):bool { return preg_match('/^[A-Z]{2}$/', $code); }
	private function isValidName($name):bool { return true; } // https://stackoverflow.com/questions/31391516/preg-match-words-with-accents

	public function code():string { return $this->code; }
	public function name():string { return $this->name; }
} 




?>
