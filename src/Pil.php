<?php
namespace HSIT\DataStructure;

use HSIT\DataStructure\Valuta;

/**
 * Data structure to handle Pil info 
 *
 * @param float $pil
 * @param Valuta $valuta
 *
 * @return true, if every check is passed, false otherwise
 */

class Pil {
	private float $pil;
	private Valuta $valuta;

	function __construct(float $pil,
				Valuta $valuta){

		$this->valuta = $valuta;
		$this->pil = $pil;
	}

	function __destruct(){ }

	function __toString(){ 
		return sprintf("pil: %.2f %s", 
			$this->pil, 
			$this->valuta); 
	}

	public function pil():float { return $this->pil; }
	public function valuta():Valuta { return $this->valuta; }
} 




?>
