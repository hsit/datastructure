<?php
namespace HSIT\DataStructure;

/**
 * Data structure to convert Magnitude in different scales
 *
 * @param Magnitude $magnitude		Magnitude
 * @param Author $author		Author of magnitude  (some Author need a different algorithm to convert value)
 *
 * @return true, if every check is passed, false otherwise
 */

class MagnitudeConverter {
	private Magnitude $magnitude;
	private Author $author;

	function __construct( 
			Magnitude $magnitude, 
			Author $author) {

		$this->magnitude = $magnitude;
		$this->author = $author;
	}

	function __destruct(){ }

	function __toString(){ 
		return sprintf("[%s] %s", $this->author , $this->magnitude);
	}

	public function ml(): float{
		switch($this->magnitude->typeLowerCase()){
			case 'mb': 
			case 'md': return 0.938 * (new MagnitudeConverter($this->magnitude, $this->author))->mw() + 0.154; break;
			case 'mw': return 0.938 * $this->magnitude->value() + 0.154; break;
			case 'ml': 
			default: return $this->magnitude->value(); break;

		}
	}

	public function mw(): float{
		switch($this->magnitude->typeLowerCase()){
			//2024-05-03: changing Md->Mw conversion for SURVEY-INGV-OV 
			//$this->Mw = 1.718 * $this->Md - 1.904;
			case 'md': return ('SURVEY-INGV-OV' == $this->author)
					? 0.82 * $this->magnitude->value() + 0.61
					: 1.472 * $this->magnitude->value() - 1.49;
					break;
			case 'ml': return 1.066 * $this->magnitude->value() - 0.164; break;
			case 'mb': return exp(0.719 + 0.212 * $this->this->magnitude->value())-0.737; break;
			case 'mw': 
			default: return $this->magnitude->value(); break;
		}
	}

	public function md(): float{
		switch($this->magnitude->typeLowerCase()){
			case 'ml': return 0; break;
			case 'mb': return 0; break;
			case 'mw': return 0; break;
			case 'md': 
			default: return $this->magnitude->value(); break;
		}
	}

	public function mb(): float{
		switch($this->magnitude->typeLowerCase()){
			case 'md': return 0; break;
			case 'ml': return 0; break;
			case 'mw': return 0; break;
			case 'mb': 
			default: return $this->magnitude->value(); break;
		}
	}

} 




?>

