<?php
namespace HSIT\DataStructure;

/**
 * Data structure to handle magnitude type and value
 *
 * @param int 	$nPeople	number of people  (must be >= 0)
 * @param float $area		area (in mq)	  (must be >0)
 *
 * @return true, if every check is passed, false otherwise
 */

class PopulationDensity {
	private int $nPeople;
	private float $area;

	function __construct( int $nPeople, float $area) {

		if( ! $this->isValidNumberPeople($nPeople) )
			throw new \InvalidArgumentException("nPeople value invalid");

		if( ! $this->isValidArea($area) )
			throw new \InvalidArgumentException("area value invalid");

		$this->nPeople = $nPeople;
		$this->area = $area;
	}

	function __destruct(){ }

	function __toString(){ 
		return sprintf("%s %.2f", $this->type, $this->value);
	}

	private function isValidNumberPeople(int $nPeople):bool { return $nPeople >= 0; } 
	private function isValidArea(float $area):bool { return $area > 0; }

	public function numberPeople(): int{ return $this->nPeople; }
	public function area(): float { return $this->area; }
	public function density(): float{ return $this->nPeople / $this->area; }
} 




?>
