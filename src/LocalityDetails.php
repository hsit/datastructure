<?php
namespace HSIT\DataStructure;

use HSIT\DataStructure\Locality;
use HSIT\DataStructure\PopulationDensity;
use HSIT\DataStructure\Pil;
use HSIT\DataStructure\UndergroundType;
use HSIT\DataStructure\Region;

/**
 * Data structure to handle Locality Details
 *
 * @param Locality 	$locality
 * @param int		$population
 * @param float		$area
 * @param Pil		$personalPil
 * @param int		$buildingNumber
 * @param UndergroundType $undergroundType
 * @param Region	$region
 *
 * @return true, if every check is passed, false otherwise
 */

class LocalityDetails implements \JsonSerializable {
	private Locality $locality;
	private PopulationDensity $populationDensity;
	private Pil $personalPil;
	private int $buildingNumber;
	private UndergroundType $undergroundType;
	private Region $region;

	function __construct(
			Locality $locality,
			PopulationDensity $populationDensity,
			Pil $personalPil,
			int $buildingNumber,
			UndergroundType $undergroundType,
			Region $region){

		$this->locality = $locality;
		$this->populationDensity = $populationDensity;
		$this->personalPil = $personalPil;
		$this->buildingNumber = $buildingNumber;
		$this->undergroundType = $undergroundType;
		$this->region = $region;
	}

	function __destruct(){ }

	function __toString(){ 
	}

	public function getLocality():Locality{ return $this->locality; }
	public function getPopulation():int { return $this->populationDensity()->getNumberPeople(); }
	public function getArea():float { return $this->populationDensity()->getArea(); }
	public function getDensity():float { return $this->populationDensity()->getDensity(); }
	public function getPersonalPil():float { return $this->personalPil->pil(); }
	public function getBuildingNumber():int { return $this->buildingNumber; }
	public function getUndergroundType():float{ return $this->undergroundType; }
	public function getRegion():string { return $this->region; }

	public function jsonSerialize(){
		return Array(
			'N' => $this->locality->name(),
			'La' => round($this->locality->point()->lat(), 7),
			'Lo' => round($this->locality->point()->lon(), 7),
			'P' => $this->populationDensity->numberPeople(),
			'D' => round($this->populationDensity->density(), 4),
			'PP' => round($this->personalPil->pil(), 2),
			'B' => $this->buildingNumber,
			'U' => $this->undergroundType->code(),
			'I' => $this->locality->istat()->code(),
			'PN' => $this->locality->province()->name(),
			'RN' => $this->region->name()
		);
	}
	
} 




?>
