<?php
namespace HSIT\DataStructure;

/**
 * Data structure to handle Valuta info 
 *
 * @param string $valuta
 *
 * @return true, if every check is passed, false otherwise
 */

class Valuta {
	private string $code;

	function __construct(string $code){

		if (! $this->isValidCode($code)) 
			throw new \InvalidArgumentException("Code invalid");

		$this->code = $code;
	}

	function __destruct(){ }

	function __toString(){ return sprintf("%s", $this->code); }

	private function isValidCode($code):bool { return preg_match('/^[A-ZA-z]{3,5}$/', $code); }

	public function code():string { return $this->code; }
	

} 




?>
