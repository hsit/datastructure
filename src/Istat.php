<?php
namespace HSIT\DataStructure;

/**
 * Data structure to handle Istat info about location
 *
 * @param int $code	Istat code
 *
 * @return true, if every check is passed, false otherwise
 */

class Istat {
	private int $code;

	function __construct(int $code) {

		if( empty($code) )
			throw new \InvalidArgumentException("Istat code invalid");

		$this->code = $code;
	}

	function __destruct(){ }

	function __toString(){ 
		return sprintf("code %d", 
			$this->code); 
	}

	public function code():int { return $this->code; }
} 




?>
