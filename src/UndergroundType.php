<?php
namespace HSIT\DataStructure;

/**
 * Data structure to handle Locality Details
 *
 * @param string	$type
 *
 * @return true, if every check is passed, false otherwise
 */

class UndergroundType {
	private string $code;
	private float $correction;

	function __construct(string $code) {

		if( empty($code) )
			throw new \InvalidArgumentException("Code invalid");

		if( ! $this->isValidCode($code) )
			throw new \InvalidArgumentException("Code invalid");

		$this->code = $code;
		$this->setCorrection();
	}

	function __destruct(){ }

	function __toString(){ 
		return sprintf("code: %s, correction: %.6f", 
			$this->code, 
			$this->correction); 
	}

	private function isValidCode(string $code):bool { return preg_match('/[A-D]/', $code); }
	private function setCorrection():void {
		switch($this->code){
			case 'A': $this->correction = 0; break;
			case 'B': $this->correction = 0.143778; break;
			case 'C': $this->correction = 0.231064; break;
			case 'D': $this->correction = 0.187402; break;
		}
	}

	public function code():string { return $this->code;}
	public function correction():float { return $this->correction;}
} 




?>
